const {defineConfig} = require("cypress");
module.exports = defineConfig({
    e2e: {
        setupNodeEvents(on, config) {
// load the localstorage plugin
            require("cypress-localstorage-commands/plugin")(on, config)
            config.baseUrl = `http://whoop.go.yo.fr`
            return config
        },
    },
});
