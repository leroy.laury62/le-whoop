beforeEach(() => {
    cy.restoreLocalStorage() // entre chaque `it` restaure le local storage
    cy.visit('/wiki.php') // requiert également un rafraichissement de page
    cy.wait(500)
})

describe('story1', () => {
    it('link', () => {
        cy.get('.WikiMenu > :nth-child(1) > a').should('have.text', 'Info service (VIH)')
        cy.get('.WikiMenu > :nth-child(1) > a').should('have.attr', 'href', 'www.sida-info-service.org')
    })
})
