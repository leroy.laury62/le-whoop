<?php
require_once "init_autoload.php";
include "./pages/head.php";
include "./pages/header.php";
?>

<body>
    <div id="quizzer">
        <!-- start Quiz button -->
        <div class="start_btn"><button>Commencer le Quiz</button></div>

        <!-- Info Box -->
        <div class="info_box">
            <div class="info-title"><span>Règles du quiz</span></div>
            <div class="info-list">
                <div class="info">1. Vous aurez <span>30 secondes</span> par questions</div>
                <div class="info">2. Une fois que vous avez sélectionné une réponse, elle ne peut pas être décochée</div>
                <div class="info">3. Vous ne pouvez pas sélectionner une réponse une fois le temps terminé</div>
                <div class="info">4. Vous obtiendrez des points selon le nombre de réponses correctes</div>
            </div>
            <div class="buttons">
                <button class="quit">Quitter Quiz</button>
                <button class="restart">Continuer</button>
            </div>
        </div>

        <!-- Quiz Box -->
        <div class="quiz_box">
            <header>
                <div class="title">Quiz</div>
                <div class="timer">
                    <div class="time_left_txt">Temps restant</div>
                    <div class="timer_sec">30</div>
                </div>
                <div class="time_line"></div>
            </header>
            <section>
                <div class="que_text">
                    <!-- Insertion question -->
                </div>
                <div class="option_list">
                    <!-- Insertion options -->
                </div>
            </section>

            <!-- footer of Quiz Box -->
            <footer class="foot">
                <div class="total_que">
                    <!-- Insertion compteur questions -->
                </div>
                <button class="quit_btn">Quitter Quiz</button>
                <button class="next_btn">Suivant</button>
            </footer>
        </div>

        <!-- Result Box -->
        <div class="result_box">
            <div class="icon">
                <i class="fas fa-crown"></i>
            </div>
            <div class="complete_text">Vous avez terminé le quiz !</div>
            <div class="score_text">
                <!-- Insertion résultats score -->
            </div>
            <div class="buttons">
                <button class="restart">Rejouer</button>
                <button class="quit">Quitter Quiz</button>
            </div>
        </div>

        <script src="JS/questions.js"></script>

        <script src="JS/quiz.js"></script>
    </div>
</body>
</html>


<?php include "./pages/footer.php"?>