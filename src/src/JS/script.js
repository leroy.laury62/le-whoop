var ImmParSec;
var ImmParClic;


/*INITIALISATION DES Imms*/
var Imm=0;
var ImmSave=parseFloat(localStorage.nbImm);
if(parseInt(localStorage.nbImm)>0){
	Imm=Imm+ImmSave;
}

/*Initialisation des armes*/
var effetTPE=0.1;
var prixTPE=20;
var TPE= 0;
var TPESave = parseInt(localStorage.nbTPE);
if(parseInt(localStorage.nbTPE)>0){
    TPE=TPE+TPESave;
}


var prixDepistage=50;
var Depistage=0;
var DepistageSave= parseInt(localStorage.nbDepistage);
if(parseInt(localStorage.nbDepistage)>0){
    Depistage = Depistage+DepistageSave;
}

var effetMedocs=2;
var prixMedocs=150;
var Medocs=0;
var MedocsSave= parseInt(localStorage.nbMedocs);
if(parseInt(localStorage.nbMedocs)>0){
    Medocs=Medocs+MedocsSave;
}

var prixPreservatif=300;
var Preservatif=0;
var PreservatifSave= parseInt(localStorage.nbPreservatif);
if(parseInt(localStorage.nbPreservatif)>0){
    Preservatif = Preservatif+PreservatifSave;
}

var effetPREP=20;
var prixPREP=500;
var PREP=0;
var PREPSave= parseInt(localStorage.nbPREP);
if(parseInt(localStorage.nbPREP)>0){
    PREP=PREP+PREPSave;
}

var effetAbstinence=100;
var prixAbstinence=5000;
var Abstinence=0;
var AbstinenceSave= parseInt(localStorage.nbAbstinence);
if(parseInt(localStorage.nbAbstinence)>0){
    Abstinence=Abstinence+AbstinenceSave;
}


$(document).ready(function(){
	/*****RESET********/

	$("#reset").click(function(){

		Imm=0;
        TPE=0;
        Depistage=0;
		Medocs=0;
        Preservatif=0;
        PREP=0;
        Abstinence=0;

		localStorage.setItem("nbImm", Imm);

        /*objets */
        localStorage.setItem("nbTPE", TPE);
		localStorage.setItem("nbDepistage",Depistage);
        localStorage.setItem("nbMedocs",Medocs);
        localStorage.setItem("nbPreservatif",Preservatif);
        localStorage.setItem("nbPREP",PREP);
        localStorage.setItem("nbAbstinence",Abstinence);

		document.location.reload(true);

	});

    /*****Imm*******/

    $("#affichage").val(Imm);
	
    $("#immunite").click(function(){
        console.log("test");
        Imm=Imm+ImmParClic; 
        console.log(Imm);
    });

    /******************************/
	/*GESTIONS DES OBJETS*/
    /******************************/
    
    $("#nbTPE").val(TPE+" TPE");
    $("#TPEB").click(function(){
        if(Imm >= prixTPE){
            TPE= TPE+1;
            Imm = Imm-prixTPE;
        }

        $("#nbTPE").val(TPE+" TPE");
    })

    $("#nbDepistage").val(Depistage+" Depistage");
    $("#DepistageB").click(function(){
        if(Imm >= prixDepistage){
            Depistage= Depistage+1;
            Imm = Imm-prixDepistage;
        }
        $("#nbDepistage").val(Depistage+" Depistage");
    })

    $("#nbMedocs").val(Medocs+" Medicaments");
    $("#MedocsB").click(function(){
        if(Imm >= prixMedocs){
            Medocs= Medocs+1;
            Imm = Imm-prixMedocs;
        }
        $("#nbMedocs").val(Medocs+" Medicaments");
    })

    $("#nbPreservatif").val(Preservatif+" Preservatifs");
    $("#PreservatifB").click(function(){
        if(Imm >= prixPreservatif){
            Preservatif= Preservatif+1;
            Imm = Imm-prixPreservatif;
        }

        $("#nbPreservatif").val(Preservatif+" Preservatifs");
    })

    $("#nbPREP").val(PREP+" PREP");
    $("#PREPB").click(function(){
        if(Imm>=prixPREP){
            PREP = PREP+1;
            Imm= Imm-prixPREP;
        }
        $("#nbPREP").val(PREP+" PREP");
    })

    $("#nbAbstinence").val(Abstinence+" Abstinence");
    $("#AbstinenceB").click(function(){
        if(Imm>=prixAbstinence){
            Abstinence= Abstinence+1;
            Imm= Imm-prixAbstinence;
        }
        $("#nbAbstinence").val(Abstinence+" Abstinence");
    })

});




	

/******************************/
	/*GESTIONS DES REFRESH*/
/******************************/

setInterval(function(){    
	ImmParSec=TPE*effetTPE+Medocs*effetMedocs+PREP*effetPREP+Abstinence*effetAbstinence;
	ImmParSec=ImmParSec;
	ImmParClic=1+Depistage*1+Preservatif*5;
	ImmParClic=ImmParClic;
	/*CHAMPIONS*/
	localStorage.setItem("nbImm", Imm);
    localStorage.setItem("nbTPE",TPE);
    localStorage.setItem("nbDepistage",Depistage);
    localStorage.setItem("nbMedocs",Medocs);
    localStorage.setItem("nbPreservatif",Preservatif);
    localStorage.setItem("nbPREP",PREP);
    localStorage.setItem("nbAbstinence",Abstinence);

    /* Affichages */

    var strImm = new String(Math.round(Imm));
    var compteur = 0;
    var strAffichageTempo="";
    var strAffichage="";

    for(let i=strImm.length; i>0;i--){
        var chiffre = strImm.substring(i-1,i);
        strAffichageTempo = strAffichageTempo+chiffre;
        compteur = compteur+1;
        if (compteur == 3){
            strAffichageTempo = strAffichageTempo+" ";
            compteur=0;
        }
    }
    for(let i=strAffichageTempo.length;i>0;i--){
        var chiffre = strAffichageTempo.substring(i-1,i);
        strAffichage = strAffichage+chiffre;
    }

    Imm = Imm+(ImmParSec/10);
    $("#affichage").val(strAffichage);
    $("#parSec").val(Math.round(ImmParSec*100)/100 + " Immunité/S");
    $("#parClic").val(Math.round(ImmParClic*100)/100 + " Immunité/Click");

}, 100);
