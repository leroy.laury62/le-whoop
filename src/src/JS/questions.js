// creating an array and passing the number, questions, options, and answers
let questions = [
    {
    numb: 1,
    question: "Que veut dire VIH ?",
    answer: "Virus de l'Immunodéficience Humaine",
    options: [
        "Virus de l'Immunodéficience Humaine",
        "Vie Infectieuse Humaine",
        "Virus Invisible Humainement",
        "Virus Interrompu Humainement"
    ]
  },
    {
    numb: 2,
    question: "Quels sont les moyens de transmission du VIH ?",
    answer: "Sang contaminé, sperme, sécrétions vaginales, lait maternel",
    options: [
      "Sang contaminé, sperme, lait maternel",
      "Sperme, sécrétions vaginales, salive",
      "Sang contaminé, sperme, sécrétions vaginales, lait maternel",
      "Sang contaminé, salive, sperme, sécrétions vaginales"
    ]
  },
    {
    numb: 3,
    question: "Comment se protéger des IST ?",
    answer: "Utiliser un préservatif",
    options: [
      "Prendre la pilule",
      "Utiliser un préservatif",
      "Le retrait avant éjaculation",
      "Se laver plusieurs fois le sexe"
    ]
  },
    {
    numb: 4,
    question: "Quel est le meilleur moyen de savoir si on a une IST ?",
    answer: "Se faire dépister",
    options: [
      "Se renseigner sur internet",
      "Acheter un test en pharmacie",
      "Se faire dépister",
      "Observer son sexe"
    ]
  },
    {
    numb: 5,
    question: "Qu'est ce que le TPE ?",
    answer: "Traitement post-exposition",
    options: [
      "Traitement pré-exposition",
      "Traitement post-exposition",
      "Traitement pre-érection",
      "Traitement préventif"
    ]
  },
    {
    numb: 6,
    question: "Le risque de transmission lors d'une fellation est :",
    answer: "Très élevé",
    options: [
      "Faible",
      "Moyen",
      "Elevé",
      "Très élevé"
    ]
  },
];