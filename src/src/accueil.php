<?php
require_once "init_autoload.php";
include "./pages/head.php";
include "./pages/header.php";?>

<link href="css/styleAccueil.css" rel="stylesheet">
<body>

    <div id="topImg">
        <h2 id="mainBvn">Bienvenue</h2>
        <h4 id="subTitle">Informe toi sur le sida en t'amusant</h4>
    </div>
    <div id="choose_contener">
        <div id="left">
            <h3>Wiki</h3>
            <a class="linker" href="wiki.php"><div id="choose_wiki" class="selector">
                <img id="wiki" src="img/journal.png">
            </div></a>
        </div>
        <div id="right">
        <h3>Nos Jeux</h3>
            <div id="choose_game" class="selector">
                <a href="clicker.php"><div class="game" id="1"><img class="game_pict" src="img/souris-dordinateur.png"></div></a>
                <a href="quiz.php"><div class="game" id="2"><img class="game_pict" src="img/podium.png"></div></a>
                <a href="memory.php"><div class="game" id="3"><img class="game_pict" src="img/jeuxsociete.png"></div></a>
                <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ"><div class="game" id="4"><img class="game_pict" src="img/puzzle.png"></div></a>
            </div>
        </div>
    </div>
</body>

<?php
include "./pages/footer.php"?>

