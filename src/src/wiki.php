<?php
require_once "init_autoload.php";
include "./pages/head.php";
include "./pages/header.php";

$title = "Wiki";
$start_link = "https://";

echo '
<html>
<head>
    <title>'.$title.'</title>
</head>
<body>

    <p>Pour plus d\'informations, voici quelques liens :</p> 
    <ul class="WikiMenu">
        <li><a href="'.$start_link.'www.sida-info-service.org"> Info service (VIH) </a></li>
        <li><a href="'.$start_link.'www.sexualites-info-sante.fr"> Info sante </a></li>
        <li><a href="'.$start_link.'www.hepatites-info-service.org"> Info service (Hepatites)</a></li>
        <li><a href="'.$start_link.'www.vih-info-soignants.fr"> Info soignant (VIH) </a></li>

        <li><a href="'.$start_link.'www.ligneazur.org"> Ligneazur </a> </li>
        <li><a href="'.$start_link.'www.instagram.com/sida_info_service"> Info service (Instagram) </a></li>
        <li><a href="'.$start_link.'www.instagram.com/sexualites_info_sante"> Info sante (Instagram)</a> </li>
        <li><a href="'.$start_link.'www.facebook.com/SidaInfoService">Info Service (Facebook)</a></li>

        <li><a href="'.$start_link.'istclic.fr/fiche-ist"> Istclic (Informations IST)</a></li>
        
        <li><a href="'.$start_link.'questionsexualite.fr/s-informer-sur-les-infections-et-les-maladies"> Info infections et maladies</a></li>
        
    </ul>
</body>
</html>';

include "./pages/footer.php";
?>