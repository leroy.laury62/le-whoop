<?php
require_once "init_autoload.php";
include "./pages/head.php";
include "./pages/header.php";

echo'
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>VIH Clicker</title>
		<link rel="stylesheet" href="css/clicker.css" type="text/css" />
		<script type="text/javascript" src="JS/jquery.js"></script>
		<script type="text/javascript" src="JS/script.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<link rel="icon" href="img/VIH_clicker.jpg" />
	</head>

	<body>
	<div id="gen">
		<div id="g">
			<div id="titre">
				<p>VIH Clicker</p>
				<input type="button" id="reset" value="RESET TOTAL"></input>
			</div>
			
			<div id="jeu">
				<div id="clicker">
					<div id="affichageImmunite">
						<div id="nbImmunite">
							<p>Immunités</p>
						</div>
						<input type="text" id="affichage"  disabled="disabled" value=""></input></br>
					</div>
					<input type="button" id="immunite" ></input>
				</div>
			</div>
            <p> Cliquez sur le Virus pour gagnez de l\'immunité et achetez des contraceptions pour le terasser !</p>
		</div>
	<div id="d">
            <div id="achats">
				<div id="TPE" class="subAchat">
					<input type="button" class="bouton" id="TPEB"></input>
					<div id="infoTPE" >
						<input type="text" id="nbTPE"  disabled="disabled" ></input>
						<input type="text" id="prixTPE"  disabled="disabled" value="prix: 20 Immunités"></input>
						<input type="text" id="effTPE"  disabled="disabled" value="0.1 Immunité/s"></input>
					</div>
				</div>

                <div id="Depistage" class="subAchat">
					<input type="button" class="bouton" id="DepistageB"></input>
					<div id="infoDepistage" >
						<input type="text" id="nbDepistage"  disabled="disabled" ></input>
						<input type="text" id="prixDepistage"  disabled="disabled" value="prix: 50 Immunités"></input>
						<input type="text" id="effDepistage"  disabled="disabled" value="1 Immunités/clic"></input>
					</div>
				</div>
                
                <div id="Medocs" class="subAchat">
					<input type="button" class="bouton" id="MedocsB"></input>
					<div id="infoMedocs" >
						<input type="text" id="nbMedocs"  disabled="disabled" ></input>
						<input type="text" id="prixMedocs"  disabled="disabled" value="prix: 150 Immunités"></input>
						<input type="text" id="effMedocs"  disabled="disabled" value="2 Immunité/s"></input>
					</div>
				</div>

                <div id="Preservatif" class="subAchat">
					<input type="button" class="bouton" id="PreservatifB"></input>
					<div id="infoPreservatif" >
						<input type="text" id="nbPreservatif"  disabled="disabled" ></input>
						<input type="text" id="prixPreservatif"  disabled="disabled" value="prix: 300 Immunités"></input>
						<input type="text" id="effPreservatif"  disabled="disabled" value="5 Immunités/clic"></input>
					</div>
				</div>

                <div id="PREP" class="subAchat">
					<input type="button" class="bouton" id="PREPB"></input>
					<div id="infoPREP" >
						<input type="text" id="nbPREP"  disabled="disabled" ></input>
						<input type="text" id="prixPREP"  disabled="disabled" value="prix: 500 Immunités"></input>
						<input type="text" id="effPREP"  disabled="disabled" value="20 Immunité/s"></input>
					</div>
				</div>

                <div id="Abstinence" class="subAchat">
					<input type="button" class="bouton" id="AbstinenceB"></input>
					<div id="infoAbstinence">
						<input type="text" id="nbAbstinence"  disabled="disabled" ></input>
						<input type="text" id="prixAbstinence"  disabled="disabled" value="prix: 5000 Immunités"></input>
						<input type="text" id="effAbstinence"  disabled="disabled" value="100 Immunité/s"></input>
					</div>
				</div>
        </div>
	</div>
	</div>
    </body>
</html>
';

include "./pages/footer.php";
?>