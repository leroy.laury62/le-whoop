Nuit de l'info 1er décembre 2022.

Sujet: https://www.nuitdelinfo.com/MA35U6z262hXyaCd/La_Nuit_de_l_Info_2022_-_Sujet.pdf

Groupe: Le Whoop

Membres: Guillaume Pestelle, Maxime Stawski, Axelle Mathias, Juliette Thouillez, Zineb Touijer, Leroy Laury

Défis choisis: Faisons de l'Open Data ! | MOVAI CODE : INTERFACE | Tester ce n'est pas douter | Easter egg | Faites connaître votre application au grand public !

## Documentation:
### Faisons de l'OpenData !:
Data open source utilisé : https://www.data.gouv.fr/fr/datasets/deces-par-sida-vih/

Utilisé puis convertie pour notre jeu du "juste prix"/"c'est plus c'est moins" en format json dans le dossier data via le script datas.ipynb.
Malheureusement non implenté.

### Easter egg:

Pas mal de références culturel (et de blague) dans le quizz.

Notamment une référence à harry potter, gandalf, matrix...
(update de dernière heure: on a pas eu le temps d'utiliser nos datas dans
notre code de quiz, donc malheureusement ces easters eggs seront tellement bien caché que personne ne les trouvera)

D'autre références cachés dans le mode mauvaie.

### MOVAI CODE : INTERFASSE:

On vous conseille de vous intéresser à l'équipe derrière tout ça ;)

### Tester ce n'est pas douter:

Implémentation de cypress pour les tests end to end.

Header et footer testés, page d'accueil en dehors des boutons jeu testée.

### Présentation projet:
Pas le temps malheureusement.
